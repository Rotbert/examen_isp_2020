import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.*;
import java.util.*;

public class test extends JFrame {
    JLabel info1,info2;
    JTextField text1,text2;
    JButton buton1;

    test(){

        setTitle("Test");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,300);
        setVisible(true);
    }

    public void init(){

        this.setLayout(null);
        int width=80;int height = 20;

        info1 = new JLabel("Intrare");
        info1.setBounds(10, 50, width, height);

        info2 = new JLabel("Iesire");
        info2.setBounds(10, 100,width, height);

        text1 = new JTextField();
        text1.setBounds(70,50,width, height);

        text2 = new JTextField();
        text2.setBounds(70,100,width, height);

        buton1 = new JButton("Calculeaza");
        buton1.setBounds(10,150,width, height);

        buton1.addActionListener(new calculeaza());


        add(info1);add(info2);add(text1);add(text2);add(buton1);
    }

    public static void main(String[] args) {
        new test();
    }

    public static void afisare(String f, int x, int y) throws IOException
    {
        FileWriter afis = new FileWriter("C:\\Users\\ROBERT\\Desktop" + f);
        afis.write("Numarul de cuvinte: " + x + "\n" );
        afis.write("Numarul de caractere: " + y);
        afis.close();
    }

    class calculeaza implements ActionListener{

        public void actionPerformed(ActionEvent e)  {

            String date = null;
            String fisier1 = test.this.text1.getText();
            String fisier2 = test.this.text2.getText();

            File read = new File("C:\\Users\\ROBERT\\Desktop" + fisier1);
            try (Scanner citire = new Scanner(read)) {
                while(citire.hasNextLine())
                { date = citire.nextLine();}
            }
            catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }

            int cuvinte = 0, litere = 0;
            char x;

            for(int i = 0; i < date.length(); i++)
            {
                x = date.charAt(i);
                if(x != ' ') litere++;
                else cuvinte++;
            }
            cuvinte++;

            try {
                afisare(fisier2, cuvinte, litere);
            } catch (IOException e1) {
                e1.printStackTrace();
            }

        }

    }

}