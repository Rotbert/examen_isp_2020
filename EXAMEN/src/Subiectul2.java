import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class Subiectul2 extends JFrame {

        JLabel egl;
        JTextField text1;
        JTextField text2;
        JTextField text3;
        JButton suma;

        Subiectul2() {
            setTitle("Application");
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            init();
            setSize(250, 250);
            setVisible(true);
        }

        public void init() {

            this.setLayout(null);
            int width = 80;
            int height = 20;

            egl = new JLabel("Numar caractere: "); //AM ADAUGAT SI UN LABEL
            egl.setBounds(10, 130, 250, height);

            text1 = new JTextField();
            text1.setBounds(10, 50, 200, height);

            text2 = new JTextField();
            text2.setBounds(10, 90, 200, height);

            text3 = new JTextField();
            text3.setBounds(130, 130, width, height);
            text3.setEditable(false);

            suma = new JButton("Calculati suma");
            suma.setBounds(10, 170, 200, height);

            suma.addActionListener(new Button());

            add(egl);
            add(text1);
            add(text2);
            add(text3);
            add(suma);

        }

        public static void main(String[] args) {
            new Subiectul2();
        }

        class Button implements ActionListener {

            public void actionPerformed(ActionEvent e) {
                String t1, t2;
                int s = 0;
                t1 = Subiectul2.this.text1.getText();
                t2 = Subiectul2.this.text2.getText();

                for(int i = 0; i<t1.length(); i++){
                    s++;
                }

                for(int i = 0; i<t2.length(); i++){
                    s++;
                }

                Subiectul2.this.text3.setText(String.valueOf(s));

            }

        }

    }

