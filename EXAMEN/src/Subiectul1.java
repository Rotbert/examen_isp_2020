interface C{
    void x();
}

class A{
    public void met(B b){
        System.out.println("method met called");
    }
}

class B implements C{
    private long t;
    E e = new E();
    public void x(){
        e.metG(5);
        System.out.println("method x called");
    }

    public static void main(String[] args) {
        B b = new B();
        b.x();
    }
}

class D{
    B b = new B();
}

class E{
    G g = new G();
    F f = new F();
    public void metG(int i){
        f.metA();
    }
}

class F{
    public void metA(){
        System.out.println("method metA called");
    }
}

class G{
    String g;
}
